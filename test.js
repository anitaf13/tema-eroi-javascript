
class heroes {
    constructor(id, nume, prenume, numeDeErou, superPutere){
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.numeDeErou = numeDeErou;
        this.superPutere= superPutere;
    }

    arataDate(){
        return [
            this.id,
            this.nume,
            this.prenume,
            this.numeDeErou,
            this.superPutere
        ]
    }
}
const url="http://localhost:3000/heroes/?_embed=superpowers"
const heroesArray = [];
const listheroes=document.querySelector(".card")
const editPost = document.querySelector(".edit-post");

document.getElementById("afiseaza").addEventListener("click", display )
function display () {
    fetch( url, { method: "GET" })
    .then(function (raspuns) {
        return raspuns.json();
    })
    .then(function (raspunsTipJson){
        console.log('raspunsTipJson:', raspunsTipJson);
        output = "";
        raspunsTipJson.forEach(function(hero, index) {
            console.log(`eroul de la indexul ${index} este ${hero.numeDeErou}`);
           heroesArray.push(
               new heroes (
                   hero.id,
                   hero.nume,
                   hero.prenume,
                   hero.numeDeErou,
                   hero.superpowers[0].superPutere
               )
           );
           output += `<ul class="list-group list-group-flush">
           <li class="list-group-item">Hero:${hero.id}</li>
           <li class="list-group-item">Nume:${hero.nume}</li>
           <li class="list-group-item">Prenume:${hero.prenume}</li>
           <li class="list-group-item">Nume de Erou: ${hero.numeDeErou}</li>
           <li class="list-group-item">Super Putere: ${hero.superpowers[0].superPutere}</li>
         </ul>
         <div class="card-footer">
         <button type="button"  id ="edit-${hero.id}" class="btn btn-primary edit-post" data-bs-toggle="modal" data-bs-target="#modal-form">Update ${hero.numeDeErou}</button>
         <button type="button"  id="delete-${hero.id}" class="btn btn-danger delete">Delete</button>
         </div>`; 
         listheroes.innerHTML = output;
        });
        const allUpdate = document.querySelectorAll(".edit-post");
        console.log(allUpdate);
        allUpdate.forEach(function (button) {
            button.addEventListener("click", update)
        })
        console.log(heroesArray);
        const allDelete = document.querySelectorAll(".delete")
        allDelete.forEach(function (button) {
          button.addEventListener("click", sterge)
        })
        })
    }

   function update(event) {
    const id = Number(event.target.id.split("-").at(-1));
    const selectedHero = heroesArray.find((hero) => hero.id === id);
    console.log(selectedHero);
    const modalBody = document.getElementById("formular");
    modalBody.innerHTML = generateform(selectedHero);
    const saveEdit = document.getElementById("editChanges")
    saveEdit.addEventListener("click", function () {
      submitHeroChanges(id)
    })
   }

function submitHeroChanges(id) {
  const form = document.querySelector("#formular form");
  const nume = form[1].value;
  const prenume = form[2].value;
  const numeErou =form[3].value;
  const putere = form[4].value;
  const patchURL = `http://localhost:3000/heroes/${id}`
  const superPowerPatchUrl = `http://localhost:3000/superpowers/${id}`
  console.log(id, nume, prenume, numeErou, putere);
  const PatchHeroes = 
  fetch(patchURL,{
    method: "PATCH",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({
       nume, prenume, numeDeErou:numeErou
    })
  });
  const PatchPowers = 
  fetch(superPowerPatchUrl,{
    method: "PATCH",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({
        superPutere:putere
    })
  })
  Promise.all([PatchHeroes,PatchPowers])
  .then(function (raspuns) {
    return raspuns.json();
})
}

function generateform(hero) {
  return `<form class="row g-3 id="formular">
    <div class="col-md-6">
      <label for="hero-id" class="form-label">Hero</label>
      <input type="text" class="form-control" id="hero-id" value="${hero.id}">
    </div>
    <div class="col-md-6">
      <label for="nume" class="form-label">Nume</label>
      <input type="text" class="form-control" id="nume" value="${hero.nume}">
    </div>
    <div class="col-12">
      <label for="prenume" class="form-label">Prenume</label>
      <input type="text" class="form-control" id="prenume" value="${hero.prenume}">
    </div>
    <div class="col-12">
      <label for="numeDeErou" class="form-label">Nume de Erou</label>
      <input type="text" class="form-control" id="numeDeErou" value="${hero.numeDeErou}">
    </div>
    <div class="col-md-6">
      <label for="superPutere" class="form-label">Super Putere</label>
      <input type="text" class="form-control" id="superPutere" value="${hero.superPutere}">
    </div>
  </form>`   
}

const addHero = document.getElementById("add-hero");
addHero.addEventListener("click", function(e){
  const form = document.querySelector("#add-form form")
  const nume = form[1].value;
  const prenume = form[2].value;
  const numeErou =form[3].value;
  const putere = form[4].value;
  const id = Math.random().toString(10).slice(2, 7);
  const postHeroUrl= `http://localhost:3000/heroes/`;
  const postPowerUrl = `http://localhost:3000/superpowers/`
  console.dir(form)
  const postHero =
  fetch(postHeroUrl,{
    method:"POST",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify ({ 
      id,
      nume: nume, 
      prenume: prenume, 
      numeDeErou: numeErou
    }),
  })
  const postPower= 
  fetch(postPowerUrl,{
    method: "POST",
    headers: {"Content-Type": "application/json"},
    body: JSON.stringify({
        superPutere:putere,
        heroId: id
    })
  })
  Promise.all([postHero,postPower])
  
})

function sterge(event) {
  const id = Number(event.target.id.split("-").at(-1));
  const fetchH1 = fetch(`http://localhost:3000/heroes/${id}` ,
      {method: 'DELETE'});
  const fetchSP1 = fetch(`http://localhost:3000/superpowers/${id}` ,
      {method: 'DELETE'});
  Promise.all([fetchH1, fetchSP1])
  
  }

 
